CURDIR=$(shell pwd)
INSTDIR=/usr/local/bin

install: tdf

update: tdf

tdf:	.FORCE
	cp -fp $(CURDIR)/tdf $(INSTDIR)/tdf

clean:
	rm -f $(INSTDIR)/tdf

.FORCE:



