#!/usr/bin/env python3

import sys
import socket
import pprint



CSV_HEADER='Taiga Mount,Allocation(TB),Used(KB),BlockSoft(KB),BlockHard(KB),BlockGrace,Files,FilesSoft,FilesHard,FilesGrace'

HOST='taiga-nfs.ncsa.illinois.edu'
PORT=5555

options = {}
options = { 'human' : False,
            'csv' : False,
            'header' : True,
          }

TERABYTE = 1024 ** 4
GIGABYTE = 1024 ** 3
MEGABYTE = 1024 ** 2

#/taiga/ncsa/cavl,164926744166400,106442209432,161061273600,161061273600,-,20488513,90000000,90000000,-

def print_the_header():
    print("%-30s    %8s    %8s   %7s   %8s   %4s   %12s" % ( 'Taiga Export', 'Alloc(T)', 'Quota(T)', 'Used(T)', 'Avail(T)', 'Use%', 'Files' ))

def print_raw_header():
    print("%-30s    %16s   %13s   %13s   %13s   %4s   %12s" % ( 'Taiga Export', 'Alloc(kbytes)', 'Quota(kbytes)', 'Used(kbytes)', 'Avail(kbytes)', 'Use%', 'Files' ))

def print_raw( ara ):
    if options['header']:
       print_raw_header()
       options['header'] = False

    alloc = int(ara[1]) * GIGABYTE
    avail = int(ara[3]) - int(ara[2])
    upct = 100 - ( ( int(ara[3]) - int(ara[2]) ) * 100 ) / int(ara[3])
    print("%-30s    %16s   %13s   %13s   %13s   %3.0f%1s   %12s" % ( ara[0], str(alloc), ara[3], ara[2], avail, upct, '%', str(ara[6]) ))

def print_human_readable( ara ):
    if options['header']:
       print_the_header()
       options['header'] = False

    bused = int(ara[2]) / GIGABYTE
    bquota = int(ara[3]) / GIGABYTE
    avail = bquota - bused
    upct = 100 - ( ( bquota - bused ) * 100 ) / bquota

    print("%-30s    %8s   %9.1f   %7.1f   %8.1f   %3.0f%1s   %12s" % ( ara[0], ara[1], bquota, bused, avail, upct, '%', str(ara[6]) ))


def print_help():
    my_help = """

    Usage: tdf [ --human | --csv ]  --debug  --help

    This utility will print out the usage information for your mapped Taiga exports:

    > tdf
    Taiga Export                         Alloc(kbytes)   Quota(kbytes)    Used(kbytes)   Avail(kbytes)   Use%          Files
    /taiga/ncsa/cavl                      161061273600    161061273600    111208277820     49852995780    69%       22952882

    > tdf -h
    Taiga Export                      Alloc(T)    Quota(T)   Used(T)   Avail(T)   Use%          Files
    /taiga/ncsa/cavl                       150       150.0     103.5       46.5    69%       22931759

    > tdf --csv
    /taiga/ncsa/cavl,150,111116403012,161061273600,161061273600,-,22931759,90000000,90000000,-


    """
    print("%s" % ( my_help ) )


if __name__ == '__main__':
   for opt in sys.argv[1:]:
       if opt == '-h':
           options['human'] = True
       elif opt == '--csv':
           options['csv'] = True
       elif opt == '--debug':
           options['debug'] = True
       elif opt == '--help':
           print_help()
           sys.exit(1)

   # Establish the connection and receive the data
   try:
      with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
           s.connect((HOST, PORT))
           data = s.recv(4096)
   except:
      print("Error Connecting To: %s : %s" % ( HOST, PORT) )
      sys.exit(1)

   for line in data.splitlines():
       lineDecoded = line.decode()
       array = lineDecoded.split(',')

       if 'REJECTED' in lineDecoded:
          print(lineDecoded)
       else:
          if options['human']:
              print_human_readable( array )
          elif options['csv']:
              print(lineDecoded)
          else:
              print_raw( array )
