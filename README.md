# tdf-client

The Taiga Lustre system does not provide project level usage information in the way that IBM Spectrum Scale does.  This command will allow the user to query their project level usage instead of seeing the usage information for the entire file system.

    Usage: tdf [ --human | --csv ]  --debug  --help

    This utility will print out the usage information for your mapped Taiga exports:

    > tdf
    Taiga Export                         Alloc(kbytes)   Quota(kbytes)    Used(kbytes)   Avail(kbytes)   Use%          Files
    /taiga/ncsa/cavl                      161061273600    161061273600    111208277820     49852995780    69%       22952882

    > tdf -h
    Taiga Export                      Alloc(T)    Quota(T)   Used(T)   Avail(T)   Use%          Files
    /taiga/ncsa/cavl                       150       150.0     103.5       46.5    69%       22931759

    > tdf --csv
    /taiga/ncsa/cavl,150,111116403012,161061273600,161061273600,-,22931759,90000000,90000000,-


Installation Instructions:

> git clone https://git.ncsa.illinois.edu/set/tdf-client.git

> cd tdf-client

> make install     

   or 

> copy the tdf script to a directory in your path
    
